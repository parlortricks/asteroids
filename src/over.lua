function over_init()
    _update = over_update
    _draw = over_draw
end -- over_init

function over_update()
end -- over_update

function over_draw()
    cls()
    stats()    
end -- over_draw
