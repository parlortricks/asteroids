local player_ship
function player_init()
    player_ship = {
        position = {
            x=64,
            y=64
        },
        rotation = 0,
        rotation_speed = 0.02,
        velocity = {
            speed=0,
            direction=0
        },
        acceleration = 0.05,
        scale = 1,
        color = 8,
        fill = false,
        points = {
            {x=8,y=0},
            {x=-8,y=6},
            {x=-4,y=0},
            {x=-8,y=-6},
            {x=8,y=0}
        }
    }

end -- player_init

function player_update()
    if btn(button.left) then
        player_ship.rotation += player_ship.rotation_speed
    end

    if btn(button.right) then
        player_ship.rotation -= player_ship.rotation_speed
    end

    if btn(button.up) then
        player_thrust()
    end

    player_ship.rotation = keep_angle_in_range(player_ship.rotation)
    player_ship.position = move_point_by_velocity(player_ship)
end -- player_update

function player_draw()
    draw_vec_shape(player_ship)
end -- player_draw

function player_thrust()
    local acceleration = {
        speed = player_ship.acceleration,
        direction = player_ship.rotation
    }

    player_ship.velocity = add_vectors(player_ship.velocity, acceleration)
    printh("velocity="..dump(player_ship.velocity))
end -- player_thrust


