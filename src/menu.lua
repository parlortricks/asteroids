function menu_init()
    _update = menu_update
    _draw = menu_draw
end -- menu_init

function menu_update()
    if btnp(❎) then
        game_init()
    end
end -- menu_update

function menu_draw()
    local title="asteroids"
    local text="press ❎ to start"
    cls()
    print("\^w\^t"..title,(_screen_width/2)-#title*4,20)
    print(text,(_screen_width/2)-#text*2,70)
    stats()    
end -- menu_draw
