_screen_width = 128
_screen_height = 128
function _init()
    debug=false
    menuitem(1, "toggle debug", function() debug = not debug end)

    menu_init()
end -- _init

function _update()
end -- _update

function _draw()
    cls()
    stats()
end -- _draw