button={
    left=       0,    --⬅️ left
    right=      1,    --➡️ right
    up=         2,    --⬆️ up
    down=       3,    --⬇️ down
    o=          4,    --🅾️ o/z
    x=          5     --❎ x
} 

function dump(o)
    if type(o) == 'table' then
       local s = '{ '
       for k,v in pairs(o) do
          if type(k) ~= 'number' then k = '"'..k..'"' end
          s = s .. '['..k..'] = ' .. dump(v) .. ','
       end
       return s .. '} '
    else
       return tostring(o)
    end
 end

function draw_vec_shape(shape)
    local first_point = true
    local last_point
    local rotated_point

    for index, point in ipairs(shape.points) do
        rotated_point = rotate_point(point, shape.rotation)
        if first_point then
            last_point = rotated_point
            first_point = false
        else
            line(
                (last_point.x * shape.scale) + shape.position.x,
                (last_point.y * shape.scale) + shape.position.y,
                (rotated_point.x * shape.scale) + shape.position.x,
                (rotated_point.y * shape.scale) + shape.position.y,
                shape.color
            )
            last_point = rotated_point
        end
    end
    if shape.fill then
        fill(shape.position.x, shape.position.y, shape.color)
    end
end -- draw_vec_shape

function rotate_point(point, rotation)
    local point, rotation = point, rotation
    local rx = (point.x * cos(rotation)) - (point.y * sin(rotation))
    local ry = (point.y * cos(rotation)) + (point.x * sin(rotation))

    return {x=rx, y=ry}
end -- rotate_point

function keep_angle_in_range(angle)
    if angle < 0 then
        while angle <0 do
            angle += 1.0
        end
    end

    if angle > 1.0 then
        while angle > 1.0 do
            angle -= 1.0
        end
    end

    return angle
end -- keep_angle_in_range

function move_point_by_velocity(object)
    local xs = object.velocity.speed * cos(object.velocity.direction)
    local ys = object.velocity.speed * sin(object.velocity.direction)

    local new_position = {
        x=object.position.x + xs,
        y=object.position.y + ys
    }

    return new_position
end -- move_point_by_velocity

function add_vectors(vector1, vector2)
    local vector1_components = get_vector_components(vector1)
    local vector2_components = get_vector_components(vector2)
    local rx = vector1_components.xc + vector2_components.xc
    local ry = vector1_components.yc + vector2_components.yc

    local resvector = comp_to_vector(rx,ry)

    return resvector
end -- add_vectors

function get_vector_components(vector)
    local xc = vector.speed * cos(vector.direction)
    local yc = vector.speed * sin(vector.direction)

    local components = {
        xc = xc,
        yc = yc
    }

    return components
end -- get_vector_components

function comp_to_vector(x,y)
    local magnitude = sqrt((x * x) + (y * y))
    local direction = atan2(y, x)
    direction = keep_angle_in_range(direction)
    local vector = {
        speed = magnitude,
        direction = direction
    }
    printh(dump(vector))

    return vector
end

