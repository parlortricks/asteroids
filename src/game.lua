function game_init()
    _update = game_update
    _draw = game_draw

    player_init()
end -- game_init

function game_update()
    player_update()
end -- game_update

function game_draw()
    cls()
    player_draw()
    stats()    
end -- game_draw
