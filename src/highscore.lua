function highscore_init()
    _update = highscore_update
    _draw = highscore_draw
end -- highscore_init

function highscore_update()
end -- highscore_update

function highscore_draw()
    cls()
    stats()    
end -- highscore_draw
